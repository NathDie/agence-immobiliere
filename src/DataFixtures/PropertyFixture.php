<?php

namespace App\DataFixtures;

use App\Entity\Property;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PropertyFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 100; $i++) {
            $property = new Property();
            $property
                ->setTitle('Bien' . $i)
                ->setDescription('Une description')
                ->setSurface(50)
                ->setRoom(2)
                ->setBedroom(1)
                ->setHeat(2)
                ->setCity('Orchies')
                ->setAdress($i . ' ' . 'rue du machin')
                ->setPostalCode('59310')
                ->setPrice(110000)
                ->setFloor(2);
            $manager->persist($property);
            $manager->flush();
        }
    }
}
