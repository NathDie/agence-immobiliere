<?php

namespace App\Controller\Admin;

use App\Entity\Option;
use App\Form\OptionType;
use App\Repository\OptionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminOptionController extends AbstractController
{
    /**
     * @var PropertyRepository
     */
    private $repository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(OptionRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }
    /**
     * @Route("/admin/option", name="admin.option.index")
     */
    public function index()
    {
        $options = $this->repository->findAll();
        return $this->render('admin/option/index.html.twig', [
            'controller_name' => 'AdminPropertyController',
            'options' => $options
        ]);
    }
    /**
     * @Route("/admin/option/create", name="admin.option.new")
     * @param Request $request
     */

    public function new(Request $request)
    {
        $option = new Option();
        $form = $this->createForm(OptionType::class, $option);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($option);
            $this->em->flush();
            $this->addFlash('add', 'Votre option a été crée');
            return $this->redirectToRoute('admin.option.index');
        }
        return $this->render('admin/option/new.html.twig', [
            'controller_name' => 'AdminPropertyController',
            'option' => $option,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/option/delete/{id}", name="admin.option.delete")
     * @param Option $option
     * @param Request $request
     */
    public function delete(Option $option, Request $request)
    {
        if ($this->isCsrfTokenValid('delete' . $option->getId(), $request->get('_token'))) {
            $this->em->remove($option);
            $this->em->flush();
            $this->addFlash('supp', 'Votre option a été supprimé');
            return $this->redirectToRoute('admin.option.index');
        } else {
            $this->addFlash('error', 'Le jeton CSRF est invalide. Veuillez renvoyer le formulaire');
            return $this->redirectToRoute('admin.option.index');
        }
    }

    /**
     * @Route("/admin/option/{id}", name="admin.option.edit")
     * @param Option $option
     * @param Request $request
     */
    public function edit(Option $option, Request $request)
    {
        $form = $this->createForm(OptionType::class, $option);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash('edit', 'Votre option a été modifié');
            return $this->redirectToRoute('admin.option.index');
        }
        return $this->render('admin/option/edit.html.twig', [
            'controller_name' => 'AdminPropertyController',
            'option' => $option,
            'form' => $form->createView()
        ]);
    }
}
