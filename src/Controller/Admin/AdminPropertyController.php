<?php

namespace App\Controller\Admin;

use App\Entity\Property;
use App\Entity\ImageProperty;
use App\Form\ImagePropertyType;
use App\Form\PropertyType;
use App\Repository\PropertyRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class AdminPropertyController extends AbstractController
{
    /**
     * @var PropertyRepository
     */
    private $repository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(PropertyRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }
    /**
     * @Route("/admin", name="admin.property.index")
     */
    public function index()
    {
        $properties = $this->repository->findAll();
        return $this->render('admin/property/index.html.twig', [
            'controller_name' => 'AdminPropertyController',
            'properties' => $properties
        ]);
    }
    /**
     * @Route("/admin/property/create", name="admin.property.new")
     * @param Request $request
     */

    public function new(Request $request)
    {
        $prop = new Property();
        $form = $this->createForm(PropertyType::class, $prop);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $images = $form->get('imageProperties')->getData();

            foreach ($images as $image) {
                // On génère un nouveau nom de fichier
                $fichier = md5(uniqid()) . '.' . $image->guessExtension();

                // On copie le fichier dans le dossier uploads
                $image->move(
                    $this->getParameter('images_directory'),
                    $fichier
                );

                // On crée l'image dans la base de données
                $img = new ImageProperty();
                $img->setFilename($fichier);
                $unedate = new \DateTime('now');
                $img->setUpdatedAt($unedate);
                $prop->addImageProperty($img);
            }


            $this->em->persist($prop);
            $this->em->flush();
            $this->addFlash('add', 'Votre annonce a été crée');
            return $this->redirectToRoute('admin.property.index');
        }
        return $this->render('admin/property/new.html.twig', [
            'controller_name' => 'AdminPropertyController',
            'property' => $prop,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/property/delete/{id}", name="admin.property.delete")
     * @param Property $property
     * @param Request $request
     */
    public function delete(Property $property, Request $request)
    {
        $data = json_decode($request->getContent(), true);
        if ($this->isCsrfTokenValid('delete' . $property->getId(), $request->get('_token'))) {
            $this->em->remove($property);
            $this->em->flush();
            $this->addFlash('supp', 'Votre annonce a été supprimé');
            return $this->redirectToRoute('admin.property.index');
        } else {
            $this->addFlash('error', 'Le jeton CSRF est invalide. Veuillez renvoyer le formulaire');
            return $this->redirectToRoute('admin.property.index');
        }
    }

    /**
     * @Route("/admin/property/{id}", name="admin.property.edit")
     * @param Property $property
     * @param Request $request
     */
    public function edit(Property $property, Request $request)
    {
        $form = $this->createForm(PropertyType::class, $property);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $images = $form->get('imageProperties')->getData();

            foreach ($images as $image) {
                $fichier = md5(uniqid()) . '.' . $image->guessExtension();
                $image->move(
                    $this->getParameter('images_directory'),
                    $fichier
                );
                $img = new ImageProperty();
                $img->setFilename($fichier);
                $unedate = new \DateTime('now');
                $img->setUpdatedAt($unedate);
                $property->addImageProperty($img);
            }
            $this->em->flush();
            $this->addFlash('edit', 'Votre annonce a été modifié');
            return $this->redirectToRoute('admin.property.index');
        }
        return $this->render('admin/property/edit.html.twig', [
            'controller_name' => 'AdminPropertyController',
            'property' => $property,
            'form' => $form->createView()

        ]);
    }

    /**
     * @Route("/admin/property/images/{id}", name="admin_property_delete_images", methods={"DELETE"})
     * @param ImageProperty $image
     * @param Request $request
     */
    public function deleteImages(ImageProperty $image, Request $request)
    {
        $data = json_decode($request->getContent(), true);
        if ($this->isCsrfTokenValid('delete' . $image->getId(), $data['_token'])) {
            $nom = $image->getFilename();
            unlink($this->getParameter('images_directory') . '/' . $nom);

            $em = $this->getDoctrine()->getManager();
            $em->remove($image);
            $em->flush();

            // On répond en json
            return new JsonResponse(['success' => 1]);
        } else {
            return new JsonResponse(['error' => 'Token Invalide'], 400);
        }
    }

    /**
     * @Route("/admin/property/image/{id}", name="admin_property_delete_image", methods={"DELETE"})
     * @param Property $property
     * @param Request $request
     */
    public function deleteImage(Property $property, Request $request)
    {
        $data = json_decode($request->getContent(), true);
        if ($this->isCsrfTokenValid('delete' . $property->getId(), $data['_token'])) {
            $nom = $property->getFilename();
            unlink($this->getParameter('image_directory') . '/' . $nom);

            $property = $property->setFilename("");
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return new JsonResponse(['success' => 1]);
        } else {
            return new JsonResponse(['error' => 'Token Invalide'], 400);
        }
    }
}
