<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class AdminUserController extends AbstractController
{
    /**
     * @var PropertyRepository
     */
    private $repository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserRepository $repository, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {
        $this->repository = $repository;
        $this->em = $em;
        $this->encoder = $encoder;
    }
    /**
     * @IsGranted("ROLE_ADMIN", message="No access! Get out!")
     * @Route("/admin/user", name="admin.user.index")
     */
    public function index()
    {
        $user = $this->repository->findAll();
        return $this->render('admin/user/index.html.twig', [
            'controller_name' => 'AdminPropertyController',
            'users' => $user
        ]);
    }
    /**
     * @IsGranted("ROLE_ADMIN", message="No access! Get out!")
     * @Route("/admin/user/create", name="admin.user.new")
     * @param Request $request
     */

    public function new(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $pwd = $this->encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($pwd);
            $this->em->persist($user);
            $this->em->flush();
            $this->addFlash('add', 'Votre user a été crée');
            return $this->redirectToRoute('admin.user.index');
        }
        return $this->render('admin/user/new.html.twig', [
            'controller_name' => 'AdminPropertyController',
            'user' => $user,
            'form' => $form->createView()
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN", message="No access! Get out!")
     * @Route("/admin/user/delete/{id}", name="admin.user.delete")
     * @param User $user
     * @param Request $request
     */
    public function delete(User $user, Request $request)
    {
        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->get('_token'))) {
            $this->em->remove($user);
            $this->em->flush();
            $this->addFlash('supp', 'Votre user a été supprimé');
            return $this->redirectToRoute('admin.user.index');
        } else {
            $this->addFlash('error', 'Le jeton CSRF est invalide. Veuillez renvoyer le formulaire');
            return $this->redirectToRoute('admin.user.index');
        }
    }

    /**
     * @IsGranted("ROLE_ADMIN", message="No access! Get out!")
     * @Route("/admin/user/{id}", name="admin.user.edit")
     * @param User $user
     * @param Request $request
     */
    public function edit(User $user, Request $request)
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $pwd = $this->encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($pwd);
            $this->em->flush();
            $this->addFlash('edit', 'Votre user a été modifié');
            return $this->redirectToRoute('admin.user.index');
        }
        return $this->render('admin/user/edit.html.twig', [
            'controller_name' => 'AdminPropertyController',
            'user' => $user,
            'form' => $form->createView()
        ]);
    }
}
